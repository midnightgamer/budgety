let budgetController = (
    function () {
        let Expense = function (id, des, value) {
            this.iD = id;
            this.description = des;
            this.value = value;
        };
        let Income = function (id, des, value) {
            this.iD = id;
            this.description = des;
            this.value = value;
        };

        let calculateTotal = function (type) {
            let sum = 0;
            data.allItems[type].forEach(function (el) {
                sum = sum + el.value;
            });
            data.totals[type] = sum;
        };

        let data = {
            allItems: {
                exp: [],
                inc: []
            },
            totals: {
                exp: 0,
                inc: 0
            },
            budget: 0,
            percentage: -1
        };
        return {
            addItem: function (type, des, val) {
                let newItem, iD;
                //Create new unique id
                if (data.allItems[type].length > 0) {
                    iD = data.allItems[type][data.allItems[type].length - 1].iD + 1;

                } else {
                    iD = 0;
                }
                // Create new expense or incomes
                if (type === 'exp') {
                    newItem = new Expense(iD, des, val)

                } else if (type === 'inc') {
                    newItem = new Income(iD, des, val)
                }
                data.allItems[type].push(newItem);
                //new el returned
                return newItem;
            },
              deleteItem : function (type , id) {
                let ids = data.allItems[type].map(function (el) {
                   return el.iD;
                });
                let index = ids.indexOf(id);

                if (index !== -1){
                    data.allItems[type].splice(index ,1)
                }
            },
            calculateBudget: function () {
                //calculate total income and expenses
                calculateTotal('exp');
                calculateTotal('inc');

                // Calculate budget
                data.budget = data.totals.inc - data.totals.exp;

                // Calculate percentage
                if (data.totals.inc > 0) {
                    data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
                } else {
                    data.percentage = -1;
                }
            },

            getBudget: function () {
                return {
                    budget: data.budget,
                    totalIncome: data.totals.inc,
                    totalExp: data.totals.exp,
                    percentage: data.percentage

                }
            },
            teting:function () {
                console.log(data)
            }
        };
    })();


// UI Controoler
let uIController = (
    function () {
        let domStrings = {
            inputType: '.add__type',
            inputDescription: '.add__description',
            inputVaule: '.add__value',
            inputBtn: '.add__btn',
            incomeContainer: '.income__list',
            expensesContainer: '.expenses__list',
            budgetLabel: '.budget__value',
            incomeLabel: '.budget__income--value',
            expensesLabel: '.budget__expenses--value',
            percentageLabel: '.budget__expenses--percentage',
            container: '.container'
        };
        return {
            getInput: function () {
                return {
                    type: document.querySelector(domStrings.inputType).value, //will get inc or exp
                    description: document.querySelector(domStrings.inputDescription).value,
                    value: parseFloat(document.querySelector(domStrings.inputVaule).value),
                }
            },


            addListItem: function (obj, type) {
                //create html string with placeholder
                let html, replacer, el;
                if (type === 'inc') {
                    el = domStrings.incomeContainer;
                    html = '  <div class="item clearfix" id="inc-%id%">\n' +
                        '                            <div class="item__description">%description%</div>\n' +
                        '                            <div class="right clearfix">\n' +
                        '                                <div class="item__value">+ %value%</div>\n' +
                        '                                <div class="item__delete">\n' +
                        '                                    <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>';
                } else if (type === 'exp') {
                    el = domStrings.expensesContainer;
                    html = '' +
                        ' <div class="item clearfix" id="exp-%id%">\n' +
                        '                            <div class="item__description">%description%</div>\n' +
                        '                            <div class="right clearfix">\n' +
                        '                                <div class="item__value">- %value%</div>\n' +
                        '                                <div class="item__percentage">21%</div>\n' +
                        '                                <div class="item__delete">\n' +
                        '                                    <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>';
                }
                //replace placeholder text
                replacer = html.replace('%id%', obj.iD);
                replacer = replacer.replace('%description%', obj.description);
                replacer = replacer.replace('%value%', obj.value);
                //inserting item to DOM
                document.querySelector(el).insertAdjacentHTML('beforeend', replacer)
            },

            clearFields: function () {
                let fields = document.querySelectorAll(domStrings.inputDescription + ',' + domStrings.inputVaule);

                let fieldsArray = Array.prototype.slice.call(fields);

                fieldsArray.forEach(function (el) {
                    el.value = "";
                });
                fieldsArray[0].focus();
            },
            dom: function () {
                return domStrings;
            },
            displayBudget: function (obj) {
                document.querySelector(domStrings.budgetLabel).textContent = obj.budget;
                document.querySelector(domStrings.incomeLabel).textContent = obj.totalIncome;
                document.querySelector(domStrings.expensesLabel).textContent = obj.totalExp;
                if (obj.percentage > 0) {
                    document.querySelector(domStrings.percentageLabel).textContent = obj.percentage + '%';
                } else {
                    document.querySelector(domStrings.percentageLabel).textContent = '---';

                }
            },


        };
    }
)();

// Event Handdler
let controller = (function (budgetCtr, uICtr) {
    let evetListiners = function () {
        let domStrings = uICtr.dom();
        document.querySelector(domStrings.inputBtn).addEventListener('click', ctrAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrAddItem();
            }
        });
        document.querySelector(domStrings.container).addEventListener('click', ctrDeleteItem)
    };
    //Updating Budget
    let updateBudget = function () {
        //Calculate budget
        budgetCtr.calculateBudget();

        let budget = budgetCtr.getBudget();

        //return budget

        uICtr.displayBudget(budget);
    };

    let ctrAddItem = function () {
        let input = uIController.getInput();

        if (input.description !== "" && !isNaN(input.value) && input.value > 0) {
            let newItem = budgetCtr.addItem(input.type, input.description, input.value);

            uICtr.addListItem(newItem, input.type);

            uICtr.clearFields();

            updateBudget();
        }
    };

  ctrDeleteItem = function (event) {

        let itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if (itemID) {
            let splitId = itemID.split('-');
            let type = splitId[0];
            let ID = splitId[1];
            ID = parseInt(ID);
            //delete from data structure
            budgetCtr.deleteItem(type,ID)
            //delete from ui

            //update budget
        }
    };

    return {
        init: function () {
            uICtr.displayBudget({
                budget: 0,
                totalIncome: 0,
                totalExp: 0,
                percentage: 0
            });
            evetListiners();
        }
    };
})(budgetController, uIController);
controller.init();
